import config_defaults
import csv
import guitools
import json
from matplotlib import pyplot
from math import *
import numpy as np
import PIL
import pprint
import sys

args = sys.argv

debug = False

data_file_path = ""
config_file_path = ""
save_path = ""


def unpack_nested_list(collection, index):
    new_list = []
    for i in collection:
        new_list.append(i[index])
    return new_list


def get_func_data_points(func="", xmin=-10, ymin=-10, xmax=10, ymax=10, resolution=1, domain=['x'], range=['y']):
    expr_str = func.split('=')[1].replace('^', '**')
    data_points = []
    for x in np.arange(xmin, xmax, resolution):
        try:
            y = eval(expr_str)
        except:
            continue
        in_domain_range = True
        for dom_str in domain:
            if not eval(dom_str):
                in_domain_range = False
        for ran_str in range:
            if not eval(ran_str):
                in_domain_range = False
        if y < ymin or y > ymax or not in_domain_range:
            continue
        else:
            data_points.append((x, y))
    return data_points


def hex_to_color(h):
    return tuple(int(h.lstrip('#')[j:j + 2], 16) for j in (0, 2, 4))


def generate_config(chart_type, path):
    if not hasattr(config_defaults, chart_type):
        print('No default config ', chart_type, ". Availible options:")
        pprint.pprint(object=list(filter(lambda x: not x.startswith('__'), dir(config_defaults))), indent=2)
    else:
        f = open(path, 'w+')
        json.dump(getattr(config_defaults, chart_type), f)
        f.close()


def cat_config(chart_type):
    if not hasattr(config_defaults, chart_type):
        print('No default config ', chart_type, ". Availible options:")
        pprint.pprint(object=list(filter(lambda x: not x.startswith('__'), dir(config_defaults))), indent=2)
    else:
        pprint.pprint(object=getattr(config_defaults, chart_type), indent=2)


def plot_function(config_dict):
    chart_opts = config_dict['chart_options']['chart_area']
    pyplot.title(config_dict['chart_options']['chart_title']['text'])
    pyplot.title(config_dict['chart_options']['axes']['vertical']['title'], loc='left')
    pyplot.title(config_dict['chart_options']['axes']['horizontal']['title'], loc='right')
    pyplot.grid(config_dict['chart_options']['grid']['lines_vertical'], axis='y')
    pyplot.grid(config_dict['chart_options']['grid']['lines_horizontal'], axis='x')
    for func_json in config_dict['chart_options']['functions']:
        linestr = func_json['equation']
        if debug:
            print(linestr, chart_opts['xmin'], chart_opts['ymin'], chart_opts['xmax'], chart_opts['ymax'],
                  chart_opts['resolution'])
        points = get_func_data_points(linestr, int(chart_opts['xmin']), int(chart_opts['ymin']),
                                      int(chart_opts['xmax']),
                                      int(chart_opts['ymax']), chart_opts['resolution'],
                                      chart_opts['functions']['domain'], chart_opts['functions']['range'])
        x = unpack_nested_list(points, 0)
        y = unpack_nested_list(points, 1)
        pyplot.plot(x, y, color=func_json['color'], linewidth=chart_opts['line_width'])
    pyplot.show()


def col(arr_table, idx):
    data = []
    for row in arr_table:
        data.append(row[idx])
    return data


def plot_scatter(config_dict):
    chart_opts = config_dict['chart_options']['chart_area']
    pyplot.title(config_dict['chart_options']['chart_title']['text'])
    pyplot.title(config_dict['chart_options']['axes']['vertical']['title'], loc='left')
    pyplot.title(config_dict['chart_options']['axes']['horizontal']['title'], loc='right')
    pyplot.grid(config_dict['chart_options']['grid']['lines_vertical'], axis='y')
    pyplot.grid(config_dict['chart_options']['grid']['lines_horizontal'], axis='x')
    table_data = read_data(data_file_path)
    lw = int(chart_opts['line_width'])
    if not bool(chart_opts['connect_points']):
        lw = 0
    pyplot.plot(col(table_data, 0), col(table_data, 1), linewidth=lw, marker='o')
    pyplot.show()


def read_data(fname):
    csvfile = open(fname, 'r')
    r = csv.reader(csvfile)
    data = []
    for row in r:
        data.append(row)
    return data


skiparg = 1  # skips first arg which is the module name
for i in range(len(args)):
    if skiparg > 0:
        skiparg -= 1
        continue
    if args[i] == "-h" or args[1] == "-?" or args[1] == "-help":
        print('Python dataviewer help:')
        print('Simple chart generator for python 3 using matplotlib.')
        print('Usage: python3 dataviewer.py [options]')
        print('===================================================================================')
        print('Availible options: ')
        print('-h, -?, -help                   Shows this help message')
        print('-v, -debug, -verbose            Enables debug output for the current run')
        print('-g, -genconfig <type> <path>    Generates a JSON config file for the given chart type')
        print('-t, -template, -default <type>  Prints the default config for a graph type')
        print('-d, -data <path>                Sets the path to a CSV data table')
        print('-C, -config <path>              Sets the path for the chart config file')
        print('-s, -save [path]                Saves the generated chart to an image file')
        print('-n, -nogui                      Don\'t create the plot and tools GUIs')
        break
    elif args[i] == "-d" or args[i] == "-data":
        skiparg = 1
        data_file_path = args[i + 1]
    elif args[i] == "-C" or args[i] == "-config":
        skiparg = 1
        config_file_path = args[i + 1]
    elif args[i] == "-g" or args[i] == "-genconfig":
        skiparg = 2
        generate_config(args[i + 1], args[i + 2])
    elif args[i] == "-s" or args[i] == "-save":
        skiparg = 1
        save_path = args[i + 1]
    elif args[i] == "-v" or args[i] == "-debug" or args[i] == "-verbose":
        debug = True
    elif args[i] == "-t" or args[i] == "-template" or args[i] == "-default":
        skiparg = 1
        cat_config(args[i + 1])

if config_file_path == '':
    config_file_path = guitools.select_file()
config_obj = json.load(open(config_file_path, 'r'))

if config_obj['header']['type'] == "function":
    plot_function(config_obj)
elif config_obj['header']['type'] == "scatter":
    plot_scatter(config_obj)

if save_path != "":
    if not save_path.endswith('.png'):
        print('Image format must be png.')
    else:
        pyplot.savefig(save_path, format='png')

if debug: print('Done reading CLI args')
