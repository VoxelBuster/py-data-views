import os
from subprocess import call

print('WARNING: This must be run in a shell/tty window, not an IDE!')
print('os: ', os.name)
if "nt" in os.name:
    print('Building Windows venv.')
    os.system('rd /S /Q .\\venv\\')
    os.system('mkdir .\\venv.\\')
    os.system('.python3 -m venv .\\venv\\')
    os.system('.\\venv\\Scripts\\activate.bat')
    os.system('.\\venv\\Scripts\\pip install matplotlib numpy pillow')
elif "ix" in os.name:
    print('Building Unix venv')
    call(['rm', '-rf', './venv/'])
    call(['python3', '-m', 'venv', './venv/'])
    try:
        call(['chmod', '-R', '+x', './venv/bin/'])
    except:
        print('Changning file modes failed, try running script with sudo.')
    call(['./venv/bin/activate'])
    call(['pip', 'install', 'matplotlib', 'numpy', 'pillow'])
