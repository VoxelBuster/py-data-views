bar = {
    "header": {
        "type": "bar",
        "datafile": "",  # csv data file to be used with this config
        "author": "",  # config author
        "title": ""  # config title
    },
    "window_options": {
        "width": "auto",  # window width
        "height": "auto",  # window height
        "bg_color_1": "#ffffff",  # gradient color 1
        "bg_color_2": "#ffffff",  # gradient color 2
        "title": ""  # window title
    },
    "chart_options": {  # chart creation and drawing options
        "chart_title": {  # chart title text and formatting options
            "text": "",
            "align": "center",
            "font_size": 24,
            "font_family": "Helvetica",
            "font_color": "#000000",
            "bold": False,
            "italic": False,
            "underline": False
        },
        "chart_area": {
            "colors": [  # colors of bars, used sequentially
                {
                    "fill": "#00aaaa",
                    "outline": "#000000"
                }
            ],
            "align": "center",  # how bars should be aligned in the chart area
            "padding": 35,  # pixels between bars not in a group
            "group_padding": 10,  # pixels between bars in a group
            "show_values": False,  # show data values above points on graph
            "horizontal": False,  # make the graph a horizontal bar graph
            "group_same": True,  # group bars under the same category
            "trendline": "off"  # trendline options: off, linear, exp, quad, cubic, quart
        },
        "axes": {
            "vertical": {
                "title": "",  # axis title text
                "scale": 1,  # axis scale
                "tickmarks": True  # outside tick mark separators
            },
            "horizontal": {
                "title": "",
                "scale": 1,  # irrelevant unless the graph is flipped
                "tickmarks": True
            }
        },
        "grid": {
            "lines_horizontal": True,
            "lines_vertical": True,
        }
    }
}

horizontal_bar = {
    "header": {
        "type": "bar",
        "datafile": "",  # csv data file to be used with this config
        "author": "",  # config author
        "title": ""  # config title
    },
    "window_options": {
        "width": "auto",  # window width
        "height": "auto",  # window height
        "bg_color_1": "#ffffff",  # gradient color 1
        "bg_color_2": "#ffffff",  # gradient color 2
        "title": ""  # window title
    },
    "chart_options": {  # chart creation and drawing options
        "chart_title": {  # chart title text and formatting options
            "text": "",
            "align": "center",
            "font_size": 24,
            "font_family": "Helvetica",
            "font_color": "#000000",
            "bold": False,
            "italic": False,
            "underline": False
        },
        "chart_area": {
            "colors": [  # colors of bars, used sequentially
                {
                    "fill": "#00aaaa",
                    "outline": "#000000"
                }
            ],
            "align": "center",  # how bars should be aligned in the chart area
            "padding": 35,  # pixels between bars not in a group
            "group_padding": 10,  # pixels between bars in a group
            "show_values": False,  # show data values above points on graph
            "horizontal": True,  # make the graph a horizontal bar graph
            "group_same": True,  # group bars under the same category
            "trendline": "off"
        },
        "axes": {
            "vertical": {
                "title": "",  # axis title text
                "scale": 1,  # axis scale
                "tickmarks": True  # outside tick mark separators
            },
            "horizontal": {
                "title": "",
                "scale": 1,
                "tickmarks": True
            }
        },
        "grid": {
            "lines_horizontal": True,
            "lines_vertical": True,
        }
    }
}

scatter = {
    "header": {
        "type": "scatter",
        "datafile": "",  # csv data file to be used with this config
        "author": "",  # config author
        "title": ""  # config title
    },
    "window_options": {
        "width": "auto",  # window width
        "height": "auto",  # window height
        "bg_color_1": "#ffffff",  # gradient color 1
        "bg_color_2": "#ffffff",  # gradient color 2
        "title": ""  # window title
    },
    "chart_options": {  # chart creation and drawing options
        "chart_title": {  # chart title text and formatting options
            "text": "",
            "align": "center",
            "font_size": 24,
            "font_family": "Helvetica",
            "font_color": "#000000",
            "bold": False,
            "italic": False,
            "underline": False
        },
        "chart_area": {
            "colors": [  # colors of data points and connecting lines, used sequentially
                {
                    "fill": "#00aaaa",
                    "outline": "#000000",
                    "line": "#0000dd"
                }
            ],
            "dot_radius": 3,  # radius of data points in pixels
            "line_width": 1,  # connecting line width, useless if connect_points is False
            "show_values": False,  # show data values above points on graph
            "trendline": "off",
            "connect_points": False,  # connect the dots
            "dot_plot": False  # Show individual dots under the data point (only works in intervals of 1)
        },
        "axes": {
            "vertical": {
                "title": "",  # axis title text
                "scale": 1,  # axis scale
                "tickmarks": True  # outside tick mark separators
            },
            "horizontal": {
                "title": "",
                "scale": 1,
                "tickmarks": True
            }
        },
        "grid": {
            "lines_horizontal": True,
            "lines_vertical": True,
        }
    }
}

line = {
    "header": {
        "type": "scatter",
        "datafile": "",  # csv data file to be used with this config
        "author": "",  # config author
        "title": ""  # config title
    },
    "window_options": {
        "width": "auto",  # window width
        "height": "auto",  # window height
        "bg_color_1": "#ffffff",  # gradient color 1
        "bg_color_2": "#ffffff",  # gradient color 2
        "title": ""  # window title
    },
    "chart_options": {  # chart creation and drawing options
        "chart_title": {  # chart title text and formatting options
            "text": "",
            "align": "center",
            "font_size": 24,
            "font_family": "Helvetica",
            "font_color": "#000000",
            "bold": False,
            "italic": False,
            "underline": False
        },
        "chart_area": {
            "colors": [  # colors of data points and connecting lines, used sequentially
                {
                    "fill": "#00aaaa",
                    "outline": "#000000",
                    "line": "#0000dd"
                }
            ],
            "dot_radius": 3,  # radius of data points in pixels
            "line_width": 1,  # connecting line width, useless if connect_points is False
            "show_values": False,  # show data values above points on graph
            "trendline": "off",
            "connect_points": True,  # connect the dots
            "dot_plot": False  # Show individual dots under the data point (only works in intervals of 1)
        },
        "axes": {
            "vertical": {
                "title": "",  # axis title text
                "scale": 1,  # axis scale
                "tickmarks": True  # outside tick mark separators
            },
            "horizontal": {
                "title": "",
                "scale": 1,
                "tickmarks": True
            }
        },
        "grid": {
            "lines_horizontal": True,
            "lines_vertical": True,
        }
    }
}

dotplot = {
    "header": {
        "type": "scatter",
        "datafile": "",  # csv data file to be used with this config
        "author": "",  # config author
        "title": ""  # config title
    },
    "window_options": {
        "width": "auto",  # window width
        "height": "auto",  # window height
        "bg_color_1": "#ffffff",  # gradient color 1
        "bg_color_2": "#ffffff",  # gradient color 2
        "title": ""  # window title
    },
    "chart_options": {  # chart creation and drawing options
        "chart_title": {  # chart title text and formatting options
            "text": "",
            "align": "center",
            "font_size": 24,
            "font_family": "Helvetica",
            "font_color": "#000000",
            "bold": False,
            "italic": False,
            "underline": False
        },
        "chart_area": {
            "colors": [  # colors of data points and connecting lines, used sequentially
                {
                    "fill": "#00aaaa",
                    "outline": "#000000",
                    "line": "#0000dd"
                }
            ],
            "dot_radius": 3,  # radius of data points in pixels
            "line_width": 1,  # connecting line width, useless if connect_points is False
            "show_values": False,  # show data values above points on graph
            "trendline": "off",
            "connect_points": False,  # connect the dots
            "dot_plot": True  # Show individual dots under the data point (only works in intervals of 1)
        },
        "axes": {
            "vertical": {
                "title": "",  # axis title text
                "scale": 1,  # axis scale
                "tickmarks": True  # outside tick mark separators
            },
            "horizontal": {
                "title": "",
                "scale": 1,
                "tickmarks": True
            }
        },
        "grid": {
            "lines_horizontal": True,
            "lines_vertical": True,
        }
    }
}

line_function = {
    "header": {
        "type": "function",
        "datafile": "",
    # not required for functions, if given it will be used as the OUTPUT for the values table, not input
        "author": "",  # config author
        "title": ""  # config title
    },
    "window_options": {
        "width": "auto",  # window width
        "height": "auto",  # window height
        "bg_color_1": "#ffffff",  # gradient color 1
        "bg_color_2": "#ffffff",  # gradient color 2
        "title": ""  # window title
    },
    "chart_options": {  # chart creation and drawing options
        "chart_title": {  # chart title text and formatting options
            "text": "",
            "align": "center",
            "font_size": 24,
            "font_family": "Helvetica",
            "font_color": "#000000",
            "bold": False,
            "italic": False,
            "underline": False
        },
        "chart_area": {
            "xmin": -10,
            "xmax": 10,
            "ymin": -10,
            "ymax": 10,
            "line_width": 3,
            "resolution": 0.1,  # interval of x between data points, larger = faster, smaller = higher quality
        },
        "axes": {
            "vertical": {
                "title": "Y",  # axis title text
                "tickmarks": True  # outside tick mark separators
            },
            "horizontal": {
                "title": "X",
                "tickmarks": True
            }
        },
        "grid": {
            "lines_horizontal": True,
            "lines_vertical": True,
        },
        "functions": [
            {
                "equation": "y=x**2+x-4",
                "color": "#ff0000",
                "domain": [
                    "x"
                ],
                "range": [
                    "y"
                ]
            }
        ]
    }
}
